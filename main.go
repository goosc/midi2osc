package main

import (
	"fmt"
	"os"
	"strings"

	//"gitlab.com/gomidi/connect"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/rtmididrv"
	"gitlab.com/goosc/osc"
	"gitlab.com/metakeule/config"
)

var (
	cfg = config.MustNew("midi2osc", "0.0.1", "converts midi notes to OSC messages")

	argAddress = cfg.NewString(
		"address", "target address where the OSC messages should be sent to",
		config.Default("127.0.0.1:8000"),
		config.Shortflag('a'),
	)
	argTemplate = cfg.NewString("template", "template for the conversion",
		config.Default("v:f/channel/%channel%/note/%key%"),
		config.Shortflag('t'),
	)

	argMIDIInPort = cfg.NewInt32("port", "number of the MIDI in port",
		config.Required,
		config.Shortflag('p'),
	)

	list = cfg.MustCommand("list", "list the midi in devices").Relax("port")
)

func main() {
	err := run()

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v", err)
		os.Exit(1)
	}
}

type noteConverter struct {
	channel  uint8
	key      uint8
	velocity uint8
	//config string
	paramsrc        []rune
	paramtag        []rune
	addressTemplate string
}

func (n *noteConverter) values() (vals []interface{}) {
	for i, src := range n.paramsrc {
		var val uint8
		switch src {
		case 'k':
			val = n.key
		case 'v':
			val = n.velocity
		case 'c':
			val = n.channel
		default:
			panic("unknown paramsrc " + string(src))
		}

		switch n.paramtag[i] {

		// rotary (-1 - 1)
		case 'r':
			if val > 127 {
				val = 127
			}
			v := int(val) - 64

			vals = append(vals, float32(v)/float32(64.0))
		// string
		case 's':
			vals = append(vals, fmt.Sprintf("%d", val))
		// int
		case 'i':
			vals = append(vals, int32(val))

		// float
		case 'f':
			vals = append(vals, float32(val))

		// normalized float (0-1)
		case 'n':
			if val > 127 {
				val = 127
			}
			vals = append(vals, float32(val)/float32(127.0))

		// boolean
		case 'b':
			var v bool
			if val > 0 {
				v = true
			}
			vals = append(vals, v)

		// binary
		case 'B':
			var v int32
			if val > 0 {
				v = 1
			}
			vals = append(vals, v)

		// true
		case 'T':
			vals = append(vals, true)

		// false
		case 'F':
			vals = append(vals, false)

		}
	}

	return
}

func (n *noteConverter) path() string {
	addr := n.addressTemplate
	addr = strings.Replace(addr, "%channel%", fmt.Sprintf("%d", n.channel), -1)
	addr = strings.Replace(addr, "%key%", fmt.Sprintf("%d", n.key), -1)
	addr = strings.Replace(addr, "%velocity%", fmt.Sprintf("%d", n.velocity), -1)
	return addr
}

func newNoteConverter(channel, key, velocity uint8, config string) (n *noteConverter) {
	n = &noteConverter{}
	n.channel = channel
	n.key = key
	n.velocity = velocity
	idx := strings.Index(config, "/")
	if idx == -1 {
		panic("invalid template")
	}

	n.addressTemplate = config[idx:]
	tags := config[:idx]

	t := strings.Split(tags, ";")

	for _, tt := range t {
		n.paramsrc = append(n.paramsrc, rune(tt[0]))
		n.paramtag = append(n.paramtag, rune(tt[2]))
	}
	return
}

func errPanic(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func must(err error) {
	if err != nil {
		panic(err.Error())
	}
}

// This example expects the first input and output port to be connected
// somehow (are either virtual MIDI through ports or physically connected).
// We write to the out port and listen to the in port.
func run() error {

	err := cfg.Run()

	if err != nil {
		return err
	}

	var _c chan bool

	drv, err := rtmididrv.New()
	must(err)

	// make sure to close all open ports at the end
	defer drv.Close()

	ins, err := drv.Ins()
	must(err)

	if cfg.ActiveCommand() == list {
		printInPorts(ins)
		//		printOutPorts(outs)
		return nil
	}

	wr, err := osc.UDPWriter(argAddress.Get())
	must(err)

	in := ins[argMIDIInPort.Get()]

	config := argTemplate.Get()

	must(in.Open())

	rd := mid.NewReader(mid.NoLogger())
	rd.Msg.Channel.NoteOn = func(_ *mid.Position, channel, key, velocity uint8) {
		nc := newNoteConverter(channel, key, velocity, config)
		fmt.Fprintf(os.Stdout, "sending %s %v to %s\n", nc.path(), nc.values(), argAddress.Get())
		osc.Path(nc.path()).WriteTo(wr, nc.values()...)
	}

	rd.Msg.Channel.NoteOff = func(_ *mid.Position, channel, key, velocity uint8) {
		nc := newNoteConverter(channel, key, 0, config)
		fmt.Fprintf(os.Stdout, "sending %s %v to %s\n", nc.path(), nc.values(), argAddress.Get())
		osc.Path(nc.path()).WriteTo(wr, nc.values()...)
	}

	// listen for MIDI
	//go rd.ReadFrom(in)
	go mid.ConnectIn(in, rd)

	<-_c
	return nil
}

func printPort(port midi.Port) {
	fmt.Printf("[%v] %s\n", port.Number(), port.String())
}

func printInPorts(ports []midi.In) {
	fmt.Printf("MIDI IN Ports\n")
	for _, port := range ports {
		printPort(port)
	}
	fmt.Printf("\n\n")
}
